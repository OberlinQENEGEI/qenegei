﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class frmAjoutChambre : Form
    {
        public frmAjoutChambre()
        {
            InitializeComponent();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
             
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.raz();
        }
        private void raz()
        {
            this.cbxLstHotels.Text = "";
            this.txtDescription.Text = "";
            this.numEtage.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Persistance.ajouteChambre(int.Parse(numEtage.Text), txtDescription.Text, int.Parse(cbxLstHotels.Text));
        }

        private void frmAjoutChambre_Load(object sender, EventArgs e)
        {
            ArrayList liste = Persistance.getLesHotels();
            foreach (Hotel chambre in liste)
            {
                cbxLstHotels.Items.Add(chambre.Id);
            }
        }
    }
}
