﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpHotel
{
    class Chambre
    {
        private int Id;
        private int Etage;
        private string description;
        private int hotel;
        
        public Chambre(int unId, int unhotel, int unEtage, string unedescription)
        {
            this.Id = unId;
            this.Etage = unEtage;
            this.description = unedescription;
            this.hotel = unhotel;
        }

        public int etage { get => Etage; set => Etage = value; }
        public string Description { get => description; set => description = value; }
        public int Hotel { get => hotel; set => hotel = value; }
        }
}
